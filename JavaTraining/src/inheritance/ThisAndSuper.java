package inheritance;

class Bat{
	/*Bat(){
		System.out.println("super default method");
	}*/
	Bat(int a){
		System.out.println("super method");
	}
	static
	{
		System.out.println("super intance block");
	}
}
public class ThisAndSuper extends Bat {
	static int a;
	{
		a=90;
		System.out.println("intance block");
	}
	ThisAndSuper(){
		super(11);
		System.out.println("sub defaulat method");
	}
	ThisAndSuper(int a){
		this();
		//super(33);
		System.out.println("sub method");
	}

	public static void main(String[] args) {
		ThisAndSuper ts=new ThisAndSuper(12);
		ThisAndSuper ts1=new ThisAndSuper();

	}

}
