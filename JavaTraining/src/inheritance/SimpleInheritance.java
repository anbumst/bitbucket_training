package inheritance;

class A
{
    int i;
    void methodOne()
    {
        System.out.println("From methodOne");
    }
}
 
public class SimpleInheritance extends A
{
    int j;
    void methodTwo()
    {
        System.out.println("From methodTwo");
    }
    public static void main(String[] ar){
    	SimpleInheritance ob=new SimpleInheritance();
    	System.out.println(ob.i+" "+ob.j);
        ob.methodOne();
        ob.methodTwo();

    }
}