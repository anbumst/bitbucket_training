package inheritance;

class Paper{
	void disp(){
		System.out.println("super method");
	}
	void show(){
		System.out.println("super show method");
	}
}
public class DowncastingEx extends Paper{
	void disp(){
		System.out.println("sub method");
	}
	void add(){
		System.out.println("sub add metod");
	}
	public static void main(String[] args) {
		Paper p=new DowncastingEx(); //upcasting
		p.disp();
		p.show();
		
		DowncastingEx d=(DowncastingEx) p; //downcasting
		d.disp();
		d.add();
		d.show();
		

	}

}
