package inheritance;


class Book{
	static void show(){
		System.out.println("super class static method");
	}
	void add(){
		System.out.println("super method");
	}
	void sub(){
		System.out.println("sbu method");
	}
}
public class OverrideMethodEx extends Book{
	static void show(){
		System.out.println("sub class static method");
	}
	void add(){
		System.out.println("sub method");
	}
	public static void main(String[] args) {
		Book bb=new Book();
		OverrideMethodEx ob=new OverrideMethodEx();
		//ob.show();
		Book ob1=new OverrideMethodEx(); //upcasting
		ob1.show();
		ob1.add();
		//OverrideMethodEx ob2=(OverrideMethodEx) new Book(); //down casting
		OverrideMethodEx ob3=(OverrideMethodEx) ob1;
		ob3.add();
	}

}
