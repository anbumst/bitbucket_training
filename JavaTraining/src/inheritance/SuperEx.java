package inheritance;

class Cat{
	/*Cat(){
		System.out.println("super default method");
	}*/
	Cat(int a){
		System.out.println("super class method"+a);
	}
}
public class SuperEx extends Cat {

	SuperEx(int a) {
		super(a);
	}

	SuperEx(int a, int b) {
		super(b);
	}
	public SuperEx() {
		super(12);
	}

	public static void main(String[] args) {
		SuperEx se=new SuperEx(44);
		SuperEx se1=new SuperEx();
		SuperEx se2=new SuperEx(33,12);

	}

}
