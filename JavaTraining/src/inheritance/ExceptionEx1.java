package inheritance;

public class ExceptionEx1 {
	static void add(){
		System.out.println("handled");
	}
	public static void main(String[] args) {
		ExceptionEx2 e=new ExceptionEx2();
		try{
			e.sub();
		}
		catch(Exception e1){
			System.out.println(e1);
		}
		add();
		System.out.println("executed");
	}

}
