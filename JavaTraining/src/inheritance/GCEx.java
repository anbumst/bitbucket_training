package inheritance;

public class GCEx {
	public void finalize(){
		System.out.println("object is garbage collected");
	}

	public static void main(String[] args) {
		/*GCEx s1=new GCEx();
		GCEx s2=new GCEx();
		GCEx s3=new GCEx();
		GCEx s4=new GCEx();

		new GCEx();
		
		s1=null;
		s2=null;
		s3=s4;
		s3=null;*/
		GCEx e1=null;
		
		int[] a=new int[3];
		int[] b={1,2,3,4,5};
		a=b;
		for(int i:a){
			System.out.println(i);
		}
		
		System.gc();
	}

}
