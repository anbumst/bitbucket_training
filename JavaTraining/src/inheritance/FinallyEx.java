package inheritance;

public class FinallyEx {

	public static void main(String[] args) {
		try{
			int a=75/0;
		} catch(ArithmeticException e){
			System.out.println("exception handled");
		} finally{
			System.out.println("finally block excecuted");
		}
	}

}
