package inheritance;

public class AssignObject {
	int a;
	void add(){
		System.out.println("add method");
	}

	public static void main(String[] args) {
		AssignObject ab=new AssignObject();
		AssignObject ab1 = null;
		//ab.a=90;
		//ab1.a=78;
		ab=ab1;
		//ab=null;
		System.out.println(ab);
		System.out.println(ab1);
		//System.out.println(ab.a);
		//System.out.println(ab1.a);
		
	}

}
