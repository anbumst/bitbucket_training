package inheritance;

import mypackage.AccessModifiersEx;

public class AccessSpecifiersEx extends AccessModifiersEx{
	
	void show(){
		System.out.println(k);
		System.out.println(m);
	}

	public static void main(String[] args) {
		AccessSpecifiersEx ac=new AccessSpecifiersEx();
		//AccessModifiersEx ac=new AccessModifiersEx();
		System.out.println("protected member: "+ac.k);
		System.out.println("public member: "+ac.m);

	}

}
