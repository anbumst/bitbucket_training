package inheritance;

public class ExceptionEx2 {
	void sub(){
		try{
			int a=9/0;
		} catch(ArithmeticException e){
			System.out.println(e);
			//throw new ArithmeticException();
		}
		
	}
	public static void main(String[] args) {
		String sr="452367986";
		//String s=sr.substring(0,3)+"-"+sr.substring(3,5)+"-"+sr.substring(6);
		//System.out.println(s);
		
		StringBuilder st= new StringBuilder(sr).insert(3, '-').insert(6, '-');
		System.out.println(st);
		
	}

}
