package inheritance;

abstract class Mobile{
	static int a=87;
	void show(){
		System.out.println("abs's method");
	};
	abstract void disp();
	abstract void add();
	Mobile(){
		System.out.println("super def cons");
	}
	/*Mobile(int a){
		System.out.println("abs cons");
	}*/
}
public class AbstractClassEx extends Mobile{
	void disp(){
		System.out.println("abstract method: "+a);
	}
	AbstractClassEx(){
		//super(12);
		System.out.println("sub cons");
	}
	AbstractClassEx(int b){
		//super(12);
		System.out.println("sub para cons");
	}
	void add(){
		
	}
	public static void main(String[] args) {
		Mobile m=new AbstractClassEx();
		AbstractClassEx m1=new AbstractClassEx(12);
		m1.disp();

	}
	

}
